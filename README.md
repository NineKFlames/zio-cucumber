# zio-cucumber

This is an attempt to build something like https://gitlab.com/NineKFlames/minimal-sba-with-cucumber, but with ZIO and
Scala

The main goal of this project is to showcase that it is possible to have a Scala CRUD backend and Cucumber functional
tests in one bottle.

Stack:

* sbt for project builds and dependency management
* ZIO as effects/runtime library
* http4s as a web-server
* Circe as the JSON codec
* ~~Slick~~ doobie for DB access
* PostgreSQL as RDBMS
* Cucumber for functional tests
* STTP as the HTTP client backing tests
* GitLab CI to run the tests on merge requests

#### Acknowledgements

The articles I've found useful for tackling dev tasks can be found in the corresponding issues for this project.
TODO: form a list of them in README.
