package cheberiak.artem.ziocukes.functest.steps

import cheberiak.artem.ziocukes.functest.world.{ HttpData, ProductsTestData, World }
import cheberiak.artem.ziocukes.product._
import io.circe.generic.semiauto.{ deriveDecoder, deriveEncoder }
import io.circe.syntax.EncoderOps
import io.circe.{ Decoder, Encoder }
import io.cucumber.datatable.DataTable
import io.cucumber.scala.Implicits._
import io.cucumber.scala.{ EN, ScalaDsl }
import org.scalatest.matchers.should.Matchers

class ProductSteps extends ScalaDsl with EN with Matchers {
  private implicit val creationDecoder: Encoder[ProductCreationRequest] = deriveEncoder[ProductCreationRequest]
  private implicit val updateDecoder: Encoder[ProductUpdateRequest]     = deriveEncoder[ProductUpdateRequest]
  private implicit val productEncoder: Decoder[ProductItemResponse]     = deriveDecoder[ProductItemResponse]
  private implicit val listEncoder: Decoder[ProductListResponse]        = deriveDecoder[ProductListResponse]

  private var products: ProductsTestData = _
  private var httpData: HttpData         = _

  def this(world: World) {
    this()
    httpData = world.http
    products = world.products
  }

  Given("^a product exists with name \"(.+)\"$") { (name: String) =>
    products.dummyProduct = EnvironmentSteps.productsTestRepo.createDummyProduct(name)
  }

  Given("^products exist for names:$") { (names: DataTable) =>
    products.dummyProductsList = EnvironmentSteps.productsTestRepo.createDummyProducts(names.asScalaRawList[String])
  }

  When("^I prepare said product's ID as request path param$") { () =>
    httpData.addPathParam(products.dummyProduct.id.toString)
  }

  When("^I prepare a product update request$") { () =>
    products.productUpdateRequest = ProductUpdateRequest();
  }

  When("^I put \"(.+)\" name to the update request$") { (name: String) =>
    products.productUpdateRequest = products.productUpdateRequest.copy(name = Some(name))
  }

  When("^I put \"(.+)\" price to the update request$") { (price: Double) =>
    products.productUpdateRequest = products.productUpdateRequest.copy(price = Some(price))
  }

  When("^I prepare a PUT body with prepared product update request$") { () =>
    httpData.requestBody = products.productUpdateRequest.asJson
  }

  When("^I prepare a product creation request with \"(.+)\" name and \"(.+)\" price$") {
    (name: String, price: Double) =>
      products.productCreationRequest = ProductCreationRequest(name, price)
  }

  When("^I prepare a POST body with prepared product update request$") { () =>
    httpData.requestBody = products.productCreationRequest.asJson
  }

  Then("^the response contains a product description JSON payload$") { () =>
    products.productFromResponse =
      EnvironmentSteps.http.checkedResponseBody[ProductItemResponse](httpData.response.body)
  }

  Then("^the response contains a product descriptions list JSON payload$") { () =>
    products.productListFromResponse =
      EnvironmentSteps.http.checkedResponseBody[ProductListResponse](httpData.response.body)
  }

  Then("^the product from response has \"(.+)\" name$") { (name: String) =>
    products.productFromResponse.name shouldBe name
  }

  Then("^the product from response has \"(.+)\" price$") { (price: Double) =>
    products.productFromResponse.price shouldBe price
  }

  Then("^the product descriptions list contains (\\d+) elements$") { (expectedSize: Int) =>
    products.productListFromResponse.items should have size expectedSize
  }

  Then("^the products from the list response are the same as in the DB$") { () =>
    val fromResponseList = products.productListFromResponse.items
    val entitiesList     = fromResponseList.map(_.id).flatMap(EnvironmentSteps.productsTestRepo.findById)

    fromResponseList should have size entitiesList.length

    val fromDbList = entitiesList.map(fromEntity)

    fromResponseList should contain allElementsOf fromDbList
  }

  Then("^the product from DB has 'deleted' flag set to \"(true|false)\"$") { (isDeleted: Boolean) =>
    products.productFromDb.deleted shouldBe isDeleted
  }

  Then("^a product with id from response exists in DB$") { () =>
    val fromResponse = products.productFromResponse
    val entityOption = EnvironmentSteps.productsTestRepo.findById(fromResponse.id)

    entityOption.isDefined shouldBe true

    val entity: ProductItem         = entityOption.get
    val fromDb: ProductItemResponse = fromEntity(entity)

    fromResponse shouldBe fromDb

    products.productFromDb = entity // save the fetched product for consequent verifications
  }

  Then("^a product with id from path param exists in DB$") { () =>
    // try fetching the product by ID from response; if successful - store it as product from DB under test
    val fromDb = EnvironmentSteps.productsTestRepo.findById(products.dummyProduct.id)

    fromDb.isDefined shouldBe true

    products.productFromDb = fromDb.get
  }

  Then("^the product from DB has \"(.+)\" name$") { (name: String) =>
    products.productFromDb.name shouldBe name
  }

  Then("^the product from DB has \"(.+)\" price$") { (price: Double) =>
    products.productFromDb.price shouldBe BigDecimal.valueOf(price)
  }

  private def fromEntity(e: ProductItem): ProductItemResponse =
    ProductItemResponse(e.id, e.name, e.price, e.createdAt)
}
