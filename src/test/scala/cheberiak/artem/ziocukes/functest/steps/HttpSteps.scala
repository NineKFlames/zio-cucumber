package cheberiak.artem.ziocukes.functest.steps

import cheberiak.artem.ziocukes.exception.ExceptionResponse
import cheberiak.artem.ziocukes.functest.world.{ HttpData, World }
import cheberiak.artem.ziocukes.health.HealthResponse
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import io.circe.parser.decode
import io.cucumber.scala.{ EN, ScalaDsl }
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.matchers.should.Matchers
import sttp.model.StatusCode

import scala.concurrent.duration.DurationInt

class HttpSteps private () extends ScalaDsl with EN with Matchers {
  private implicit val healthDecoder: Decoder[HealthResponse]       = deriveDecoder[HealthResponse]
  private implicit val exceptionDecoder: Decoder[ExceptionResponse] = deriveDecoder[ExceptionResponse]

  private var health: HealthResponse           = _
  private var errorResponse: ExceptionResponse = _
  private var httpData: HttpData               = _

  def this(world: World) {
    this()
    httpData = world.http
  }

  Given("^the backend is located on localhost with random local server port$") { () =>
    EnvironmentSteps.http.initializeLocalhostServerUri()
    eventually(Timeout(10.seconds)) {
      EnvironmentSteps.http.get("/")
    }
  }

  When("^I make a GET request to \"(.+)\" endpoint$") { (endpoint: String) =>
    httpData.response = EnvironmentSteps.http.get(endpoint)
  }

  When("^I make a GET request to \"(.+)\" endpoint, parametrized with prepared path params$") { (endpoint: String) =>
    httpData.response = EnvironmentSteps.http.getParametrizedEndpoint(endpoint, httpData.pathParams)
  }

  When("^I prepare \"(.+)\" as request path param$") { (param: String) =>
    httpData.addPathParam(param);
  }

  When("^I make a PATCH request with a prepared body to \"(.+)\" endpoint, parametrized with prepared path param$") {
    (endpoint: String) =>
      httpData.response =
        EnvironmentSteps.http.patchParametrizedEndpointWithBody(endpoint, httpData.pathParams, httpData.requestBody)
  }

  When("^I make a POST request to \"(.+)\" endpoint with a prepared body$") { (endpoint: String) =>
    httpData.response = EnvironmentSteps.http.postWithBody(endpoint, httpData.requestBody);
  }

  When("^I make a DELETE request to \"(.+)\" endpoint, parametrized with prepared path param$") { (endpoint: String) =>
    httpData.response = EnvironmentSteps.http.deleteParametrizedEndpoint(endpoint, httpData.pathParams)
  }

  Then("^I get a ([0-9]{3}) response code$") { (status: Int) =>
    httpData.response.code shouldBe StatusCode(status)
  }

  Then("^the response contains a health JSON payload$") { () =>
    health = EnvironmentSteps.http.checkedResponseBody[HealthResponse](httpData.response.body)
  }

  Then("^the status is \"(.+)\"$") { (expectedStatus: String) =>
    health.status shouldBe expectedStatus
  }

  Then("^the response body is empty$") { () =>
    val body = httpData.response.body
    body shouldBe a[Right[_, _]]
    body.toOption.get shouldBe empty
  }

  Then("^the response contains an exception JSON payload$") { () =>
    errorResponse = checkedFailureResponseBody(httpData.response.body)
  }

  Then("^an exception of type \"(.+)\" was thrown$") { (expectedExceptionName: String) =>
    errorResponse.exception shouldBe expectedExceptionName
  }

  Then("^the exception contains text \"(.+)\"$") { (expectedExceptionMessage: String) =>
    errorResponse.message should include(expectedExceptionMessage)
  }

  private def checkedFailureResponseBody(body: Either[String, String]): ExceptionResponse = {
    body shouldBe a[Left[_, _]]

    val parsedJson = decode[ExceptionResponse](body.toOption.get)
    parsedJson shouldBe a[Right[_, _]]

    parsedJson.toOption.get
  }
}
