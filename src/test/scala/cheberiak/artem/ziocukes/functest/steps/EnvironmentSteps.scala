package cheberiak.artem.ziocukes.functest.steps

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import doobie.implicits._
import cheberiak.artem.ziocukes.Application
import cheberiak.artem.ziocukes.functest.world.HttpData
import cheberiak.artem.ziocukes.product.{ ProductTestRepo, ProductsDoobieTestRepo }
import com.dimafeng.testcontainers.PostgreSQLContainer
import doobie.Transactor
import doobie.implicits.toSqlInterpolator
import doobie.util.fragment.Fragment
import io.cucumber.scala.{ EN, ScalaDsl }
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.slf4j.LoggerFactory
import org.testcontainers.containers.{ PostgreSQLContainer => JavaPostgreSQLContainer }
import org.testcontainers.utility.DockerImageName
import zio.{ CancelableFuture, ExitCode, Runtime, Unsafe, ZIO, ZIOAppArgs, ZLayer }

import java.security.SecureRandom
import java.util.Properties
import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.{ Duration, DurationInt }
import scala.util.Random

class EnvironmentSteps extends ScalaDsl with EN {
  var httpData: HttpData = _

  Before {
    httpData = HttpData()
  }

  Given("^\"(.+)\" table in the DB is cleared$") { (tableName: String) =>
    eventually(Timeout(10.seconds)) {
      Fragment.const0(s"TRUNCATE TABLE $tableName").update.run.transact(EnvironmentSteps.transactor).unsafeRunSync()
    }
  }
}

object EnvironmentSteps extends ScalaDsl {
  private var cancelableServer: CancelableFuture[ExitCode] = _
  private var postgres: PostgreSQLContainer                = _
  private var transactor: Transactor[IO]                   = _

  lazy val http: HttpEngine                  = HttpEngine()
  lazy val productsTestRepo: ProductTestRepo = ProductsDoobieTestRepo(transactor)

  BeforeAll {
    val (container, tx) = launchPostgres()
    System.setProperties(postgresEnvConfig(container))

    cancelableServer = launchZioServer()

    postgres = container
    transactor = tx
  }

  AfterAll {
    Await.result(cancelableServer.cancel(), Duration.Inf)
    postgres.stop()
  }

  private def launchPostgres(): (PostgreSQLContainer, Transactor[IO]) = {
    val rnd: Random                       = new SecureRandom()
    val randomAlphanumeric: Int => String = rnd.alphanumeric.take(_).mkString
    val pgPass                            = randomAlphanumeric(20)
    val pgUser                            = randomAlphanumeric(10)
    val pgDatabase                        = s"functest-${randomAlphanumeric(10)}"
    val container                         = new PostgreSQLContainer(
      dockerImageNameOverride = Some(DockerImageName.parse("postgres:15.10").asCompatibleSubstituteFor("postgres")),
      pgPassword = Some(pgPass),
      pgUsername = Some(pgUser),
      databaseName = Some(pgDatabase)
    )

    container.start()
    val transactor = Transactor.fromDriverManager[IO]("org.postgresql.Driver", databaseUrl(container), pgUser, pgPass)

    eventually(Timeout(10.seconds)) {
      sql"SELECT 1".query[Int].option.transact(transactor).unsafeRunSync()
    }

    (container, transactor)
  }

  private def databaseUrl(container: PostgreSQLContainer): String = {
    val mappedPort = container.mappedPort(JavaPostgreSQLContainer.POSTGRESQL_PORT)
    s"jdbc:postgresql://${container.host}:$mappedPort/${container.databaseName}"
  }

  private def postgresEnvConfig(container: PostgreSQLContainer): Properties = {
    val returnValue = new Properties(3)
    returnValue.put("POSTGRES_URL", databaseUrl(container))
    returnValue.put("POSTGRES_USER", container.username)
    returnValue.put("POSTGRES_PASS", container.password)
    returnValue
  }

  private def launchZioServer(): CancelableFuture[ExitCode] = {
    val server = Unsafe.unsafe { implicit unsafe =>
      Runtime.default.unsafe.runToFuture(Application.run.provide(ZIOAppArgs.empty))
    }
    server.onComplete { outcome =>
      val logger = LoggerFactory.getLogger(EnvironmentSteps.getClass) // runs only once, so it's fine

      outcome.failed.toOption.map(_.getCause).foreach {
        case cancelled: InterruptedException =>
          logger.info(s"""ZIO app run interrupted.
                         |This may be a normal cancel() call in AfterAll, but check the cause if in doubt.
                         |Interrupt: $cancelled
                         |Cause: ${cancelled.getCause}""".stripMargin)
        case other                           => logger.error("ZIO app closed unexpectedly.", other)
      }
    }(ExecutionContext.global) // runs only once, so it's fine

    server
  }
}
