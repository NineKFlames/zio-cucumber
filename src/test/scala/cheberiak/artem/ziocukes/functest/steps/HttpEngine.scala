package cheberiak.artem.ziocukes.functest.steps

import io.circe.parser.decode
import io.circe.{ Decoder, Json }
import org.scalatest.matchers.should.Matchers
import sttp.client3._
import sttp.client3.circe._
import sttp.model.{ HeaderNames, MediaType, Uri }

class HttpEngine private () extends Matchers {
  private val backend: SttpBackend[Identity, Any] = HttpClientSyncBackend()
  private var serverUri: Uri                      = _

  private[steps] def initializeLocalhostServerUri(port: Int = 8080): Unit =
    serverUri = uri"http://localhost:$port"

  private[steps] def get(endpoint: String): Identity[Response[Either[String, String]]] =
    backend.send(basicRequest.get(prepareEndpointUri(endpoint)))

  private[steps] def getParametrizedEndpoint(
    endpoint: String,
    pathParams: List[String]
  ): Response[Either[String, String]] =
    backend.send(basicRequest.get(prepareParametrizedEndpointUri(endpoint, pathParams)))

  private[steps] def patchParametrizedEndpointWithBody(
    endpoint: String,
    pathParams: List[String],
    requestBody: Json
  ): Response[Either[String, String]] =
    backend.send(prepareJsonBodyRequest(requestBody).patch(prepareParametrizedEndpointUri(endpoint, pathParams)))

  private[steps] def postWithBody(endpoint: String, requestBody: Json): Response[Either[String, String]] =
    backend.send(prepareJsonBodyRequest(requestBody).post(prepareEndpointUri(endpoint)))

  private[steps] def deleteParametrizedEndpoint(
    endpoint: String,
    pathParams: List[String]
  ): Response[Either[String, String]] =
    backend.send(basicRequest.delete(prepareParametrizedEndpointUri(endpoint, pathParams)))

  private def prepareJsonBodyRequest(requestBody: Json) =
    basicRequest.header(HeaderNames.ContentType, Some(MediaType.ApplicationJson.toString())).body(requestBody)

  private def prepareEndpointUri(endpoint: String): Uri =
    serverUri.withPath(endpoint.split('/').toSeq)

  private def prepareParametrizedEndpointUri(endpoint: String, pathParams: List[String]): Uri =
    serverUri.withPath(endpoint.format(pathParams: _*).split('/').toSeq)

  private[steps] def checkedResponseBody[A](body: Either[String, String])(implicit decoder: Decoder[A]): A = {
    body shouldBe a[Right[_, _]]

    val parsedJson = decode[A](body.toOption.get)
    parsedJson shouldBe a[Right[_, _]]

    parsedJson.toOption.get
  }
}

object HttpEngine {
  def apply(): HttpEngine = new HttpEngine()
}
