package cheberiak.artem.ziocukes.functest.world

case class World(http: HttpData = HttpData(), products: ProductsTestData = ProductsTestData())
