package cheberiak.artem.ziocukes.functest.world

import cheberiak.artem.ziocukes.product._

class ProductsTestData private () {
  var dummyProduct: ProductItem                      = _
  var dummyProductsList: List[ProductItem]           = _
  var productFromResponse: ProductItemResponse       = _
  var productListFromResponse: ProductListResponse   = _
  var productFromDb: ProductItem                     = _
  var productUpdateRequest: ProductUpdateRequest     = _
  var productCreationRequest: ProductCreationRequest = _
}

object ProductsTestData {
  def apply(): ProductsTestData =
    new ProductsTestData()
}
