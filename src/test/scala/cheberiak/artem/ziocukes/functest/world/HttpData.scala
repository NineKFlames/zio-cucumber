package cheberiak.artem.ziocukes.functest.world

import io.circe.Json
import sttp.client3.Response

class HttpData private () {
  private var _pathParams: List[String]                   = List.empty
  private var _response: Response[Either[String, String]] = _
  var requestBody: Json                                   = _

  def response: Response[Either[String, String]] = _response

  def response_=(value: Response[Either[String, String]]): Unit =
    _response = value

  def addPathParam(param: String): Unit =
    _pathParams = _pathParams :+ param

  def pathParams: List[String] = {
    val returnValue = _pathParams
    _pathParams = List.empty
    returnValue
  }
}

object HttpData {
  def apply(): HttpData =
    new HttpData()
}
