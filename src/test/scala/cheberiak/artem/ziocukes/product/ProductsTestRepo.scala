package cheberiak.artem.ziocukes.product

import cats.effect.IO
import doobie._
import doobie.implicits._
import doobie.postgres.implicits._

import java.time.{ ZoneOffset, ZonedDateTime }
import java.util.UUID

trait ProductTestRepo {
  def createDummyProduct(name: String): ProductItem

  def createDummyProducts(names: Iterable[String]): List[ProductItem]

  def findById(id: UUID): Option[ProductItem]
}

class ProductsDoobieTestRepo private (tx: Transactor[IO]) extends ProductTestRepo {

  import cats.effect.unsafe.implicits.global

  override def createDummyProduct(name: String): ProductItem =
    createDummyProducts(List(name)).head

  override def createDummyProducts(names: Iterable[String]): List[ProductItem] =
    Update[ProductItem](s"INSERT INTO ${ProductDoobieLive.SQL.TableName.internals.sql} VALUES (?, ?, ?, ?, ?)")
      .updateManyWithGeneratedKeys[ProductItem](ProductDoobieLive.SQL.AllColumns: _*)
      .apply(names.map(dummyProductData).toList)
      .transact(tx)
      .compile
      .toList
      .unsafeRunSync()

  private def dummyProductData(name: String): ProductItem =
    ProductItem(UUID.randomUUID(), name, BigDecimal(Math.random()), ZonedDateTime.now(ZoneOffset.UTC), deleted = false)

  override def findById(id: UUID): Option[ProductItem] =
    sql"SELECT * FROM ${ProductDoobieLive.SQL.TableName} WHERE id = $id"
      .query[ProductItem]
      .option
      .transact(tx)
      .unsafeRunSync()
}

object ProductsDoobieTestRepo {
  def apply(tx: Transactor[IO]): ProductsDoobieTestRepo =
    new ProductsDoobieTestRepo(tx)
}
