Feature: Product
  As a user, I want to get product info, edit it and delete products in an auditable way.

  Background:
    Given the backend is located on localhost with random local server port
    And "product" table in the DB is cleared

  Scenario: Fetch one product info
    Given a product exists with name "TEST"
    When I prepare said product's ID as request path param
    And I make a GET request to "api/v1/product/%s" endpoint, parametrized with prepared path params
    Then I get a 200 response code
    And the response contains a product description JSON payload
    And the product from response has "TEST" name

  @ignore # https://gitlab.com/NineKFlames/zio-cucumber/-/issues/12
  Scenario Template: Fetch one product info for invalid ID
    When I prepare "<invalidId>" as request path param
    And I make a GET request to "api/v1/product/%s" endpoint, parametrized with prepared path params
    Then I get a <responseCode> response code
    And the response contains an exception JSON payload
    And an exception of type "<exceptionName>" was thrown
    And the exception contains text "<containedInException>"
    Examples:
      | invalidId                            | responseCode | exceptionName                       | containedInException                                                                 |
      | negative test                        | 500          | MethodArgumentTypeMismatchException | Failed to convert value of type 'java.lang.String' to required type 'java.util.UUID' |
      | 1                                    | 500          | MethodArgumentTypeMismatchException | Failed to convert value of type 'java.lang.String' to required type 'java.util.UUID' |
      | uuidthat-does-ntex-isti-nthedatabase | 500          | MethodArgumentTypeMismatchException | Failed to convert value of type 'java.lang.String' to required type 'java.util.UUID' |
      | 00000000-0000-0000-0000-000000000000 | 404          | ProductNotFoundException            | Product with ID 00000000-0000-0000-0000-000000000000 not found!                      |

  Scenario Template: Update product name
    Given a product exists with name "TEST"
    When I prepare said product's ID as request path param
    And I prepare a product update request
    And I put "<name>" name to the update request
    And I prepare a PUT body with prepared product update request
    And I make a PATCH request with a prepared body to "api/v1/product/%s" endpoint, parametrized with prepared path param
    Then I get a 200 response code
    And the response contains a product description JSON payload
    And the product from response has "<name>" name
    And a product with id from path param exists in DB
    And the product from DB has "<name>" name
    Examples:
      | name                 |
      | NEW_NAME_TEST        |
      | EVEN_NEWER_NAME_TEST |

  Scenario Template: Update product price
    Given a product exists with name "TEST"
    When I prepare said product's ID as request path param
    And I prepare a product update request
    And I put "<price>" price to the update request
    And I prepare a PUT body with prepared product update request
    And I make a PATCH request with a prepared body to "api/v1/product/%s" endpoint, parametrized with prepared path param
    Then I get a 200 response code
    And the response contains a product description JSON payload
    And the product from response has "<price>" price
    And a product with id from path param exists in DB
    And the product from DB has "<price>" price
    Examples:
      | price  |
      | 600    |
      | 250.50 |

  Scenario Template: Update both product name and price
    Given a product exists with name "TEST"
    When I prepare said product's ID as request path param
    And I prepare a product update request
    And I put "<price>" price to the update request
    And I put "<name>" name to the update request
    And I prepare a PUT body with prepared product update request
    And I make a PATCH request with a prepared body to "api/v1/product/%s" endpoint, parametrized with prepared path param
    Then I get a 200 response code
    And the response contains a product description JSON payload
    And a product with id from path param exists in DB
    And the product from DB has "<name>" name
    And the product from DB has "<price>" price
    Examples:
      | name                            | price  |
      | NEW_NAME_WITH_PRICE_TEST        | 999.99 |
      | EVEN_NEWER_NAME_WITH_PRICE_TEST | 800    |

  Scenario Template: Create a new product
    When I prepare a product creation request with "<name>" name and "<price>" price
    And I prepare a POST body with prepared product update request
    And I make a POST request to "api/v1/product/new" endpoint with a prepared body
    Then I get a 200 response code
    And the response contains a product description JSON payload
    And a product with id from response exists in DB
    And the product from DB has "<name>" name
    And the product from DB has "<price>" price
    Examples:
      | name                     | price  |
      | NEW_NAME_WITH_PRICE_TEST | 999.99 |
      | Name with spaces         | 999.99 |

  Scenario: Fetch list of all products
    Given products exist for names:
      | "TEST0" |
      | "TEST1" |
      | "TEST2" |
      | "TEST3" |
    When I make a GET request to "api/v1/product/list" endpoint
    Then I get a 200 response code
    And the response contains a product descriptions list JSON payload
    And the product descriptions list contains 4 elements
    And the products from the list response are the same as in the DB

  Scenario: Delete one product
    Given a product exists with name "TEST_FOR_DELETION"
    When I prepare said product's ID as request path param
    And I make a DELETE request to "api/v1/product/%s" endpoint, parametrized with prepared path param
    Then I get a 200 response code
    And the response body is empty
    And a product with id from path param exists in DB
    And the product from DB has 'deleted' flag set to "true"
