Feature: Health check
  As a developer, I want to know that the backend server is up and
  running at all should all tests fail.

  Background:
    Given the backend is located on localhost with random local server port

  Scenario: Get health status
    When I make a GET request to "health" endpoint
    Then I get a 200 response code
    And the response contains a health JSON payload
    And the status is "UP"
