CREATE TABLE IF NOT EXISTS product
(
    id         uuid,
    name       text,
    price      numeric(22, 2),
    created_at timestamptz,
    deleted    boolean,
    PRIMARY KEY (id)
)