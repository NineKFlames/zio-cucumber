package cheberiak.artem.ziocukes.health

import zio._
import zio.http._

trait HealthService {
  val routes: Routes[Any, Response]
}

object HealthServiceLive {
  val layer: TaskLayer[HealthService] = {
    ZLayer {
      ZIO.attempt {
        new HealthService {
          override val routes: Routes[Any, Response] = Routes(
            Method.GET / Root / "health" ->
              handler(Response(status = Status.Ok, body = Body.from(HealthResponse("UP"))))
          )
        }
      }
    }
  }
}
