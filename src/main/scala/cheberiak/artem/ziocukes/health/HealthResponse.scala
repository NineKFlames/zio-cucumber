package cheberiak.artem.ziocukes.health

import zio.schema.codec.{ BinaryCodec, JsonCodec }
import zio.schema.{ DeriveSchema, Schema }

case class HealthResponse(status: String)

object HealthResponse {
  implicit val schema: Schema[HealthResponse]        = DeriveSchema.gen[HealthResponse]
  implicit val zioCodec: BinaryCodec[HealthResponse] = JsonCodec.schemaBasedBinaryCodec
}
