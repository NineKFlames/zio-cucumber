package cheberiak.artem.ziocukes

import cheberiak.artem.ziocukes.config.{ DbConfigLive, ServerConfig }
import cheberiak.artem.ziocukes.db.{ FlywayLive, TransactorProviderLive }
import cheberiak.artem.ziocukes.health.HealthServiceLive
import cheberiak.artem.ziocukes.http.{ Router, RouterLive }
import cheberiak.artem.ziocukes.product.{ ProductDoobieLive, ProductServiceLive }
import com.typesafe.config.ConfigFactory
import zio._
import zio.http.Server

object Application extends ZIOAppDefault {
  private val typesafeConfig = ZLayer.scoped(ZIO.attempt(ConfigFactory.load()))
  private val serverConfig   = typesafeConfig >>> ServerConfig.layer
  private val databaseConfig = typesafeConfig >>> DbConfigLive.layer

  private val databaseTransactor = databaseConfig >+> FlywayLive.layer >+> TransactorProviderLive.layer

  private val productLayer = databaseTransactor >+> ProductDoobieLive.layer >+> ProductServiceLive.layer

  private val routerLayer = productLayer ++ HealthServiceLive.layer >+> RouterLive.layer

  override def run: ZIO[Any with ZIOAppArgs, Throwable, ExitCode] =
    ZIO.service[Router].flatMap(r => Server.serve(r.routes)).provide(routerLayer, Server.live, serverConfig)
}
