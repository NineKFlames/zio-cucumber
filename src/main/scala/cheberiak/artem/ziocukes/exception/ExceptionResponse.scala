package cheberiak.artem.ziocukes.exception

case class ExceptionResponse(message: String, exception: String)
