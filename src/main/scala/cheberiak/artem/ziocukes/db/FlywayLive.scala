package cheberiak.artem.ziocukes.db

import cheberiak.artem.ziocukes.config.DbConfig
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult
import zio.{ RLayer, ZIO, ZLayer }

object FlywayLive {
  val layer: RLayer[DbConfig, MigrateResult] = ZLayer {
    for {
      dbConfig <- ZIO.service[DbConfig]
      result   <- ZIO.attemptBlocking {
                    Flyway
                      .configure()
                      .validateMigrationNaming(true)
                      .dataSource(dbConfig.url, dbConfig.user, dbConfig.password)
                      .load()
                      .migrate()
                  }
    } yield result
  }
}
