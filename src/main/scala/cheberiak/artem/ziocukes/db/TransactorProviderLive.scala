package cheberiak.artem.ziocukes.db

import cheberiak.artem.ziocukes.config.DbConfig
import doobie._
import org.flywaydb.core.api.output.MigrateResult
import zio._
import zio.interop.catz._
import zio.interop.catz.implicits._

object TransactorProviderLive {
  val layer: ZLayer[DbConfig with MigrateResult, Throwable, Transactor[Task]] = ZLayer {
    for {
      dbConfig <- ZIO.service[DbConfig]
      _        <- ZIO.service[MigrateResult]
      tx       <- ZIO.attempt {
                    Transactor.fromDriverManager[Task](dbConfig.driver, dbConfig.url, dbConfig.user, dbConfig.password)
                  }
    } yield tx
  }
}
