package cheberiak.artem.ziocukes.product

import doobie._
import doobie.implicits._
import doobie.postgres.implicits._
import zio._
import zio.interop.catz._

import java.time.{ ZoneId, ZonedDateTime }
import java.util.UUID

trait ProductRepository {
  def getAll: Task[List[ProductItem]]

  def getOne(id: UUID): Task[ProductItem]

  def create(request: ProductCreationRequest): Task[ProductItem]

  def update(id: UUID, request: ProductUpdateRequest): Task[ProductItem]

  def delete(id: UUID): Task[Boolean]
}

final case class ProductDoobieLive private ()(implicit tx: Transactor[Task]) extends ProductRepository {

  import cheberiak.artem.ziocukes.product.ProductDoobieLive.SQL

  override def getAll: Task[List[ProductItem]] = SQL.getAll().to[List].transact(tx)

  override def getOne(id: UUID): Task[ProductItem] =
    SQL.get(id).option.transact(tx).someOrFail(ProductNotFoundException(id))

  override def create(request: ProductCreationRequest): Task[ProductItem] =
    SQL.create(request).withUniqueGeneratedKeys[ProductItem](SQL.AllColumns: _*).transact(tx)

  override def update(id: UUID, request: ProductUpdateRequest): Task[ProductItem] =
    SQL.update(id, request).withUniqueGeneratedKeys[ProductItem](SQL.AllColumns: _*).transact(tx)

  override def delete(id: UUID): Task[Boolean] =
    SQL.delete(id).withUniqueGeneratedKeys[Boolean]("deleted").transact(tx)
}

object ProductDoobieLive {
  val layer: ZLayer[Transactor[Task], Throwable, ProductRepository] =
    ZLayer {
      ZIO.service[Transactor[Task]].map(ProductDoobieLive()(_))
    }

  private[product] final object SQL {
    val TableName: Fragment      = Fragment.const0("product")
    val Id: String               = "id"
    val Name: String             = "name"
    val Price: String            = "price"
    val CreatedAt: String        = "created_at"
    val Deleted: String          = "deleted"
    val AllColumns: List[String] = List(Id, Name, Price, CreatedAt, Deleted)

    def getAll(): Query0[ProductItem] =
      sql"""SELECT *
           |FROM $TableName
           |WHERE deleted = false""".stripMargin.query[ProductItem]

    def get(id: UUID): Query0[ProductItem] =
      sql"""SELECT *
           |FROM $TableName
           |WHERE id = $id AND deleted = false""".stripMargin.query[ProductItem]

    def create(request: ProductCreationRequest): Update0 =
      sql"""INSERT INTO $TableName
           |VALUES (
           |  ${UUID.randomUUID},
           |  ${request.name},
           |  ${request.price},
           |  ${ZonedDateTime.now(ZoneId.of("UTC"))},
           |  false)""".stripMargin.update

    def update(id: UUID, product: ProductUpdateRequest): Update0 = {
      val setFragment = Fragments.set(
        Vector(
          product.name.map(n => fr"name = $n").getOrElse(Fragment.empty),
          product.price.map(p => fr"price = $p").getOrElse(Fragment.empty)
        ).filterNot(Fragment.empty == _): _*
      )

      sql"UPDATE $TableName $setFragment WHERE id = $id AND deleted = false".update
    }

    def delete(id: UUID): Update0 =
      sql"""UPDATE $TableName
           |SET deleted = true
           |WHERE id = $id AND deleted = false""".stripMargin.update
  }
}

final case class ProductNotFoundException(id: UUID) extends RuntimeException(s"Product with ID $id not found!")

case class ProductItem private (id: UUID, name: String, price: BigDecimal, createdAt: ZonedDateTime, deleted: Boolean)
    extends Serializable
