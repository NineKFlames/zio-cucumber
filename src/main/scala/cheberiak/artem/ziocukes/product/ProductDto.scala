package cheberiak.artem.ziocukes.product

import zio.schema.codec.{ BinaryCodec, JsonCodec }
import zio.schema.{ DeriveSchema, Schema }

import java.time.ZonedDateTime
import java.util.UUID

case class ProductItemResponse(id: UUID, name: String, price: BigDecimal, createdAt: ZonedDateTime)

object ProductItemResponse {
  implicit val schema: Schema[ProductItemResponse]        = DeriveSchema.gen[ProductItemResponse]
  implicit val zioCodec: BinaryCodec[ProductItemResponse] = JsonCodec.schemaBasedBinaryCodec
}

case class ProductListResponse(items: List[ProductItemResponse]) {}

object ProductListResponse {
  implicit val schema: Schema[ProductListResponse]        = DeriveSchema.gen[ProductListResponse]
  implicit val zioCodec: BinaryCodec[ProductListResponse] = JsonCodec.schemaBasedBinaryCodec
}

case class ProductCreationRequest(name: String, price: BigDecimal)

object ProductCreationRequest {
  implicit val schema: Schema[ProductCreationRequest]        = DeriveSchema.gen[ProductCreationRequest]
  implicit val zioCodec: BinaryCodec[ProductCreationRequest] = JsonCodec.schemaBasedBinaryCodec
}

case class ProductUpdateRequest(name: Option[String] = None, price: Option[BigDecimal] = None)

object ProductUpdateRequest {
  implicit val schema: Schema[ProductUpdateRequest]        = DeriveSchema.gen[ProductUpdateRequest]
  implicit val zioCodec: BinaryCodec[ProductUpdateRequest] = JsonCodec.schemaBasedBinaryCodec
}
