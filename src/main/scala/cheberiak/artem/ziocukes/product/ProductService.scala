package cheberiak.artem.ziocukes.product

import zio._
import zio.http.MediaType.application
import zio.http._

import java.util.UUID

trait ProductService {
  val routes: Routes[Any, Response]
}

object ProductService {
  val routes: URIO[ProductService, ProductService] = ZIO.service
}

case class ProductServiceLive private (repository: ProductRepository) extends ProductService {
  override val routes: Routes[Any, Response] = Routes(
    Method.GET / "list"        -> handler {
      for {
        list         <- repository.getAll
        listResponse <- asListResponse(list)
      } yield Response(status = Status.Ok, body = Body.from(listResponse)).contentType(application.json)
    },
    Method.GET / uuid("id")    -> handler { (id: UUID, _: Request) =>
      for {
        product         <- repository.getOne(id)
        productResponse <- asResponse(product)
      } yield Response(status = Status.Ok, body = Body.from(productResponse)).contentType(application.json)
    },
    Method.POST / "new"        -> handler { req: Request =>
      for {
        body            <- req.body.to[ProductCreationRequest]
        product         <- repository.create(body)
        productResponse <- asResponse(product)
      } yield Response(status = Status.Ok, body = Body.from(productResponse)).contentType(application.json)
    },
    Method.PATCH / uuid("id")  -> handler { (id: UUID, req: Request) =>
      for {
        body            <- req.body.to[ProductUpdateRequest]
        product         <- repository.update(id, body)
        productResponse <- asResponse(product)
      } yield Response(status = Status.Ok, body = Body.from(productResponse)).contentType(application.json)
    },
    Method.DELETE / uuid("id") -> handler { (id: UUID, _: Request) =>
      repository.delete(id).map(_ => Response(status = Status.Ok))
    }
  ).sandbox

  private def asResponse(p: ProductItem): UIO[ProductItemResponse] =
    ZIO.succeed(ProductItemResponse(p.id, p.name, p.price, p.createdAt))

  private def asListResponse(ps: List[ProductItem]): UIO[ProductListResponse] =
    ZIO.foreachPar(ps)(asResponse).map(ProductListResponse(_))
}

object ProductServiceLive {
  val layer: URLayer[ProductRepository, ProductService] =
    ZLayer(ZIO.service[ProductRepository].map(ProductServiceLive(_)))
}
