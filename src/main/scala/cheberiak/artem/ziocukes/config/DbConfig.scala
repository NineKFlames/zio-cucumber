package cheberiak.artem.ziocukes.config

import com.typesafe.config.Config
import zio.config.magnolia.DeriveConfig.deriveConfig
import zio.config.typesafe.TypesafeConfigProvider
import zio.{ RLayer, ZLayer }

case class DbConfig(driver: String, url: String, user: String, password: String)

object DbConfigLive {
  private implicit val conf: zio.Config[DbConfig] = deriveConfig[DbConfig]

  val layer: RLayer[Config, DbConfig] = for {
    externalConf <- ZLayer.service[Config]
    config       <- ZLayer {
                      TypesafeConfigProvider.fromTypesafeConfig(externalConf.get.getConfig("database")).load[DbConfig]
                    }
  } yield config
}
