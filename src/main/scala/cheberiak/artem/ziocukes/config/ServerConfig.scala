package cheberiak.artem.ziocukes.config

import com.typesafe.config.Config
import zio.config.magnolia.DeriveConfig.deriveConfig
import zio.config.typesafe.TypesafeConfigProvider
import zio.http.Server
import zio.{ RLayer, ZIO, ZLayer }

object ServerConfig {
  private case class ServerConfig(port: Int)

  private implicit val conf: zio.Config[ServerConfig] = deriveConfig[ServerConfig]

  val layer: RLayer[Config, Server.Config] = ZLayer {
    for {
      typesafeConf <- ZIO.service[Config]
      loadedConf   <- TypesafeConfigProvider.fromTypesafeConfig(typesafeConf.getConfig("web-server")).load
    } yield Server.Config.default.port(loadedConf.port)
  }
}
