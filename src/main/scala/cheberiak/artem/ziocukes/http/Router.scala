package cheberiak.artem.ziocukes.http

import cheberiak.artem.ziocukes.health.HealthService
import cheberiak.artem.ziocukes.product.ProductService
import zio._
import zio.http._
import zio.http.codec.PathCodec._

trait Router {
  val routes: Routes[Any, Response]
}

object Router {
  val routes: URIO[Router, Router] = ZIO.service[Router]
}

case class RouterLive private (healthService: HealthService, productService: ProductService) extends Router {
  override val routes: Routes[Any, Response] =
    healthService.routes ++ literal("api") / "v1" / "product" / productService.routes
}

object RouterLive {
  val layer: URLayer[HealthService with ProductService, Router] = ZLayer {
    for {
      hs <- ZIO.service[HealthService]
      ps <- ZIO.service[ProductService]
    } yield RouterLive(hs, ps)
  }
}
