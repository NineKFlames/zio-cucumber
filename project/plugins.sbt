addSbtPlugin("org.jetbrains.scala" % "sbt-ide-settings" % "1.1.1")
addSbtPlugin("com.waioeka.sbt" % "cucumber-plugin" % "0.3.1")
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.5.4")
