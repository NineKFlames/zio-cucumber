import sbt._

object Dependencies {

    object ZIO {
        private val zioVersion = "2.1.14"
        private val loggingVersion = "2.4.0"
        private val zioConfigVersion = "4.0.3"

        private val zio = "dev.zio" %% "zio" % zioVersion
        private val http = "dev.zio" %% "zio-http" % "3.0.1"
        private val streams = "dev.zio" %% "zio-streams" % zioVersion
        private val logging = "dev.zio" %% "zio-logging" % loggingVersion
        private val loggingSlf4j = "dev.zio" %% "zio-logging-slf4j" % loggingVersion
        private val catsInterop = "dev.zio" %% "zio-interop-cats" % "23.1.0.3"
        private val config = "dev.zio" %% "zio-config" % zioConfigVersion
        private val configTypesafe = "dev.zio" %% "zio-config-typesafe" % zioConfigVersion
        private val configMagnolia = "dev.zio" %% "zio-config-magnolia" % zioConfigVersion

        val all: Seq[ModuleID] = Seq(
            zio,
            http,
            streams,
            logging,
            loggingSlf4j,
            catsInterop,
            config,
            configTypesafe,
            configMagnolia
        )
    }

    object Cats {
        private val cats = "org.typelevel" %% "cats-core" % "2.12.0"
        private val catsEffect = "org.typelevel" %% "cats-effect" % "3.5.7"

        val all: Seq[ModuleID] = Seq(cats, catsEffect)
    }

    object Logging {
        private val slf4jVersion = "2.0.13"

        private val slf4j = "org.slf4j" % "slf4j-simple" % slf4jVersion
        private val slf4jApi = "org.slf4j" % "slf4j-api" % slf4jVersion

        val all: Seq[ModuleID] = Seq(slf4j, slf4jApi)
    }

    object Database {
        private val doobieVersion = "1.0.0-RC1"

        private val postgres = "org.postgresql" % "postgresql" % "42.7.3"
        private val doobieCore = "org.tpolecat" %% "doobie-core" % doobieVersion
        private val doobiePostgres = "org.tpolecat" %% "doobie-postgres" % doobieVersion
        private val flyway = "org.flywaydb" % "flyway-core" % "9.22.3"

        val all: Seq[ModuleID] = Seq(postgres, doobieCore, doobiePostgres, flyway)
    }

    object Testing {
        private val cucumberVersion = "7.18.1"
        private val sttpVersion = "3.9.7"

        private val cucumberCore = "io.cucumber" % "cucumber-core" % cucumberVersion % Test
        private val cucumberScala = "io.cucumber" %% "cucumber-scala" % "8.23.1" % Test
        private val cucumberJvm = "io.cucumber" % "cucumber-jvm" % cucumberVersion % Test
        private val cucumberJUnit = "io.cucumber" % "cucumber-junit-platform-engine" % cucumberVersion % Test
        private val cucumberPicocontainer = "io.cucumber" % "cucumber-picocontainer" % cucumberVersion % Test
        private val scalatest = "org.scalatest" %% "scalatest" % "3.2.19" % Test
        private val sttpClient = "com.softwaremill.sttp.client3" %% "core" % sttpVersion % Test
        private val sttpCirce = "com.softwaremill.sttp.client3" %% "circe" % sttpVersion % Test
        private val circeGeneric = "io.circe" %% "circe-generic" % "0.14.9" % Test
        private val testcontainers = "com.dimafeng" %% "testcontainers-scala-core" % "0.41.4" % Test
        private val testcontainersPostgres = "com.dimafeng" %% "testcontainers-scala-postgresql" % "0.41.4" % Test

        val all: Seq[ModuleID] = Seq(
            cucumberCore,
            cucumberScala,
            cucumberJvm,
            cucumberJUnit,
            cucumberPicocontainer,
            scalatest,
            sttpClient,
            sttpCirce,
            circeGeneric,
            testcontainers,
            testcontainersPostgres
        )
    }
}
