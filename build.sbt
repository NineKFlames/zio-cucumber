import Dependencies._

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.15"

lazy val root = (project in file("."))
    .settings(
        name := "zio-cucumber",
        libraryDependencies ++= Testing.all ++ ZIO.all ++ Database.all ++ Logging.all ++ Cats.all,
        // Allows for graceful shutdown of containers once the tests have finished running.
        // https://github.com/testcontainers/testcontainers-scala
        Test / fork := true,
        CucumberPlugin.features :=
            Path.allSubpaths((Test / resourceDirectory).value / "features").map(_._1.getPath).toList,
        CucumberPlugin.glues := List("cheberiak.artem.ziocukes.functest.steps"),
        CucumberPlugin.envProperties := Map(
            "cucumber.publish.quiet" -> "true",
            "cucumber.execution.parallel.enabled" -> "true",
            "cucumber.filter.tags" -> "not @ignore"
        )
    )
    .enablePlugins(CucumberPlugin)
